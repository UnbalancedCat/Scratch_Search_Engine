﻿#pragma once

#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include <algorithm>
#include <regex>
#include <time.h>
#include "trie.h"
#include <Windows.h>
using namespace std;

namespace sse
{
	// config
	constexpr auto DEBUG_INFO = false;
	constexpr auto PROCESS_INFO = false;
	constexpr auto DEBGU_KEY_WORDS = "china us uk";
	constexpr auto DISPLY_RANGE = 5;

	enum StartMode
	{
		cold_start,
		hot_start,
	};

	struct Doc_Index
	{
		int doc_id = -1;
		string title;
		string url;
		string content;
	};

	class Search_Engine
	{
	public:
		Search_Engine()
		{
			_doc_id = 0; _term_id = 0;
		}
		void start(string news_csv_address = "news.csv", string doc_index_csv = "doc_index.csv", string term_index_csv = "term_index.csv");
	private:
		trie::trie _dic_term;
		vector<Doc_Index> _dic_doc;
		string _news_buf_str;
		string _doc_buf_str;
		string _term_buf_str;
		int _doc_id;
		int _term_id;

		bool read_file(string news_csv_address, string doc_index_csv_address, string term_index_csv_address, StartMode mode = cold_start);
		void cut_file(string doc_index_csv_address, string term_index_csv_address, StartMode mode = cold_start);
		void search();
		
		void merge_sort(vector<Doc_Unit>& term_sort, int len);
		bool is_file_exist_stat(string& file_address);
		void logo();
	};

	void Search_Engine::start(string news_csv_address, string doc_index_csv_address, string term_index_csv_address)
	{
		StartMode mode = StartMode::cold_start;
		if(is_file_exist_stat(news_csv_address))
		{
			if (is_file_exist_stat(doc_index_csv_address) && is_file_exist_stat (term_index_csv_address))
			{
				string input;
				std::cout  << "                                                                                    \033[30;1mdefault mode: hot start\033[0m\r[Scratch Search Engine] Index files found, whether to read the index file (y or n): ";
				getline(cin, input);
				if(input == "n" || input == "no" || input == "N" || input == "NO" || input == "No") mode = StartMode::cold_start;
				else
				{
					if (input != "y" && input != "yes" && input != "Y" && input != "YES" && input != "Yes")std::cout << "[Scratch Search Engine] Executing default mode\n";
					mode = StartMode::hot_start;
				}
			}
			else mode = StartMode::cold_start;
			read_file(news_csv_address, doc_index_csv_address, term_index_csv_address, mode);
			cut_file(doc_index_csv_address, term_index_csv_address, mode);
			search();
		}
		else
			std::cout  << "[Scratch Search Engine] \033[31;1mERROR! File Missing!\033[0m Exiting program...\n";
		return;
	}
	bool Search_Engine::read_file(string news_csv_address, string doc_index_csv_address, string term_index_csv_address, StartMode mode)
	{
		clock_t start = clock();
		switch (mode)
		{
		case StartMode::cold_start:
		{
			if (PROCESS_INFO)std::cout  << "[Scratch Search Notice] Reading file news.csv..." << endl;
			ifstream read_news(news_csv_address, std::ios::binary);
			if (read_news.is_open())
			{
				if (DEBUG_INFO)std::cout  << "[\033[32;1mScratch Search Debug\033[0m] " << news_csv_address << " file open seccessfully!" << endl;
				vector<char> news_buf;
				news_buf.resize((read_news.seekg(0, std::ios::end).tellg()));
				read_news.seekg(0, std::ios::beg).read(&news_buf[0], (news_buf.size()));
				_news_buf_str.append(news_buf.begin(), news_buf.end());
				read_news.close();
				if (DEBUG_INFO)std::cout  << "[\033[32;1mScratch Search Debug\033[0m] " << news_csv_address << " file close!" << endl;
			}
			else return false;
			clock_t end = clock();
			if (PROCESS_INFO)std::cout  << "[Scratch Search Notice] time : " << ((double)end - start) / CLOCKS_PER_SEC << "s" << endl;
			return true;
		}
		case StartMode::hot_start:
		{
			vector<char> str_buf;

			if (PROCESS_INFO)std::cout  << "[Scratch Search Notice] Reading doc_index.csv..." << endl;
			ifstream read_doc(doc_index_csv_address, std::ios::binary);
			if (read_doc.is_open())
			{
				if (DEBUG_INFO)std::cout  << "[\033[32;1mScratch Search Debug\033[0m] " << doc_index_csv_address << " file open seccessfully!" << endl;
				str_buf.resize((read_doc.seekg(0, std::ios::end).tellg()));
				read_doc.seekg(0, std::ios::beg).read(&str_buf[0], (str_buf.size()));
				_doc_buf_str.append(str_buf.begin(), str_buf.end());
				if (DEBUG_INFO)std::cout  << "[\033[32;1mScratch Search Debug\033[0m] " << doc_index_csv_address << " file close!" << endl;
			}
			else return false;
			if (PROCESS_INFO)std::cout  << "[Scratch Search Notice] Reading term_index.csv..." << endl;
			ifstream read_term(term_index_csv_address, std::ios::binary);
			if (read_term.is_open())
			{
				if (DEBUG_INFO)std::cout  << "[\033[32;1mScratch Search Debug\033[0m] " << term_index_csv_address << " file open seccessfully!" << endl;
				str_buf.resize((read_term.seekg(0, std::ios::end).tellg()));
				read_term.seekg(0, std::ios::beg).read(&str_buf[0], (str_buf.size()));
				_term_buf_str.append(str_buf.begin(), str_buf.end());
				if (DEBUG_INFO)std::cout  << "[\033[32;1mScratch Search Debug\033[0m] " << term_index_csv_address << " file close!" << endl;
			}
			else return false;
			clock_t end = clock();
			if (PROCESS_INFO)std::cout  << "[Scratch Search Notice] time : " << ((double)end - start) / CLOCKS_PER_SEC << "s" << endl;
		}
		default: return false;
		}
	}
	void Search_Engine::cut_file(string doc_index_csv_address, string term_index_csv_address, StartMode mode)
	{
		clock_t start = clock();
		switch(mode)
		{ 
		case StartMode::cold_start:
		{
			string url, title, content, term;
			int term_num_pre_doc;
			char* p_head = &_news_buf_str[0];
			char* p_tail;
			char* p_term_head;
			char* p_term_tail;
			bool loop = true;

			if (_news_buf_str.substr(0, 22) == "url,title,news_content")
				p_head += 24;
			while (*p_head != '\0')
			{
				// url
				p_tail = p_head + 1;
				if (*p_head != ',' && *p_head != '\r' && *p_head != '\n')
				{
					while (*p_tail != ',')
						p_tail++;
				}
				else
				{
					p_head++;
					continue;
				}
				url = _news_buf_str.substr(int(p_head - &_news_buf_str[0]), int(p_tail - p_head));

				// title
				p_head = p_tail + 1;
				p_tail = p_head + 1;
				if (*p_head == '\"')
				{
					while (true)
					{
						if (*p_tail != '\"')
							p_tail++;
						else if (*p_tail == '\"' && *(p_tail + 1) == '\"')
							p_tail += 2;
						else
						{
							p_tail++;
							break;
						}
					}
				}
				else if (*p_head != ',')
				{
					while (*p_tail != ',')
						p_tail++;
				}
				else
				{
					p_head++;
					continue;
				}
				title = _news_buf_str.substr(int(p_head - &_news_buf_str[0]), int(p_tail - p_head));

				// content
				p_head = p_tail + 1;
				p_tail = p_head + 1;
				p_term_head = p_head;
				loop = true;
				term_num_pre_doc = 0;
				if (*p_head == '\"')
				{
					while (true)
					{
						if (*p_tail < 0)
						{
							p_tail++;
							continue;
						}
						if (*p_tail == ' ' || *p_tail == '\n')
						{
							p_term_tail = p_tail;
							while (p_term_head < p_term_tail)
							{
								if (*p_term_head < 0)
								{
									p_term_head++;
									continue;
								}
								if (isalnum(*p_term_head) == 0)p_term_head++;
								else break;
							}
							while (p_term_head < p_term_tail)
							{
								if (*(p_term_tail - 1) < 0)
								{
									p_term_tail--;
									continue;
								}
								if (isalnum(*(p_term_tail - 1)) == 0)p_term_tail--;
								else break;
							}
							if (p_term_head < p_term_tail)
							{
								term = _news_buf_str.substr(int(p_term_head - &_news_buf_str[0]), int(p_term_tail - p_term_head));
								transform(term.begin(), term.end(), term.begin(), ::tolower);
								if (term == "han")
									int a = 0;
								term_num_pre_doc++;
								auto find = _dic_term.search(term, trie::SearchMode::count);
								if (!find)
								{
									_dic_term.insert(term, _term_id++, _doc_id);
									if (PROCESS_INFO && (_term_id % 10000 == 0))std::cout  << "[Scratch Search Notice] " << _term_id << " terms have recorded" << endl;

								}
								else
									_dic_term.check(find, _doc_id);
							}
							p_term_head = p_tail + 1;
						}
						if (*p_tail != '\"')
							p_tail++;
						else if (*p_tail == '\"' && *(p_tail + 1) == '\"')
							p_tail += 2;
						else
						{
							p_tail++;
							loop = false;
						}
						if (loop == false)
						{
							break;
						}
					}
				}
				content = _news_buf_str.substr(int(p_head - &_news_buf_str[0]), int(p_tail - p_head));
				content.erase(remove(content.begin(), content.end(), '\t'), content.end());
				content.erase(remove(content.begin(), content.end(), '\n'), content.end());
				content.erase(remove(content.begin(), content.end(), '\r'), content.end());
				_dic_doc.push_back({ _doc_id, title, url, content });
				if (_doc_id == 0)_doc_buf_str += ("doc_id,url,title,content,\n");
				_doc_buf_str += (to_string(_doc_id) + "," + url + "," + title + "," + content + ",\n");

				_doc_id++;
				term_num_pre_doc++;
				p_head = p_tail + 1;
			}
			if (PROCESS_INFO)std::cout  << "[Scratch Search Notice] There are " << _doc_id << " pages in total" << endl;
			if (PROCESS_INFO)std::cout  << "[Scratch Search Notice] There are " << _term_id << " terms in total" << endl;

			if (PROCESS_INFO)std::cout  << "[Scratch Search Notice] Outputing file term_index.csv..." << endl;
			_dic_term.traversal(_term_buf_str);
			ofstream write_term_index_csv(term_index_csv_address, std::ios::binary);
			if (DEBUG_INFO && write_term_index_csv.is_open())std::cout  << "[\033[32;1mScratch Search Debug\033[0m] " << "term_index.csv" << " file open seccessfully!" << endl;
			write_term_index_csv << _term_buf_str;
			write_term_index_csv.close();
			if (PROCESS_INFO)std::cout  << "[Scratch Search Notice] Outputing file doc_index.csv..." << endl;
			if (DEBUG_INFO)std::cout  << "[\033[32;1mScratch Search Debug\033[0m] " << "term_index.csv" << " file close!" << endl;
			ofstream write_doc_index_csv(doc_index_csv_address, std::ios::binary);
			if (DEBUG_INFO && write_doc_index_csv.is_open())std::cout  << "[\033[32;1mScratch Search Debug\033[0m] " << "doc_index.csv" << " file open seccessfully!" << endl;
			write_doc_index_csv << _doc_buf_str;
			write_doc_index_csv.close();
			if (DEBUG_INFO)std::cout  << "[\033[32;1mScratch Search Debug\033[0m] " << "doc_index.csv" << " file close!" << endl;
			_dic_term.clear();

			clock_t end = clock();
			if (PROCESS_INFO)std::cout  << "[Scratch Search Notice] time : " << ((double)end - start) / CLOCKS_PER_SEC << "s" << endl;
			return;
		}
		case StartMode::hot_start:
		{
			char* p_head;
			char* p_tail;
			char* p_num_head;
			char* p_num_tail;
			bool loop = true;

			// doc_index
			{
				string doc_id, url, title, content;
				p_head = &_doc_buf_str[0];
				string doc_head = "doc_id,url,title,content,\n";
				p_head += doc_head.size();
				while (*p_head != '\0')
				{
					// doc_id
					p_tail = p_head + 1;
					while (*p_tail != ',')
						p_tail++;
					doc_id = _doc_buf_str.substr(int(p_head - &_doc_buf_str[0]), int(p_tail - p_head));
					_doc_id++;

					// url
					p_head = p_tail + 1;
					p_tail = p_head + 1;
					while (*p_tail != ',')
						p_tail++;
					url = _doc_buf_str.substr(int(p_head - &_doc_buf_str[0]), int(p_tail - p_head));

					// title
					p_head = p_tail + 1;
					p_tail = p_head + 1;
					if (*p_head == '\"')
					{
						while (true)
						{
							if (*p_tail != '\"')
								p_tail++;
							else if (*p_tail == '\"' && *(p_tail + 1) == '\"')
								p_tail += 2;
							else
							{
								p_tail++;
								break;
							}
						}
					}
					else
					{
						while (*p_tail != ',')
							p_tail++;
					}
					title = _doc_buf_str.substr(int(p_head - &_doc_buf_str[0]), int(p_tail - p_head));

					// content
					p_head = p_tail + 1;
					p_tail = p_head + 1;

					//vector
					if (*p_head == '\"')
					{
						while (true)
						{
							if (*p_tail != '\"')
								p_tail++;
							else if (*p_tail == '\"' && *(p_tail + 1) == '\"')
								p_tail += 2;
							else
							{
								p_tail++;
								break;
							}
						}
					}
					else
					{
						while (*p_tail != ',')
							p_tail++;
					}
					content = _doc_buf_str.substr(int(p_head - &_doc_buf_str[0]), int(p_tail - p_head));

					_dic_doc.push_back({ atoi(doc_id.c_str()),url,title,content });

					p_head = p_tail + 2; // 跳过 '\n'
				}
			}
			// term_index
			{
				string term_id, term;
				vector<int>frequency;
				vector<int>doc_id;
				p_head = &_term_buf_str[0];
				string term_head = "term_id,term,frequency,doc_id,\n";
				p_head += term_head.size();
				while (*p_head != '\0')
				{
					// term_id
					p_tail = p_head + 1;
					while (*p_tail != ',')
						p_tail++;
					term_id = _term_buf_str.substr(int(p_head - &_term_buf_str[0]), int(p_tail - p_head));
					_term_id++;
					// term
					p_head = p_tail + 1;
					p_tail = p_head + 1;
					if (*p_head == '\"')
					{
						while (true)
						{
							if (*p_tail != '\"')
								p_tail++;
							else if (*p_tail == '\"' && *(p_tail + 1) == '\"')
								p_tail += 2;
							else
							{
								p_tail++;
								break;
							}
						}
					}
					else
					{
						while (*p_tail != ',')
							p_tail++;
					}
					term = _term_buf_str.substr(int(p_head + 1 - &_term_buf_str[0]), int(p_tail - p_head - 2)); // 排除 ""
					
					// frequency
					frequency.resize(0);
					loop = true;
					p_head = p_tail + 1;
					p_tail = p_head + 1;
					p_num_head = p_head;
					if (*p_head == '\"')
					{
						p_head++;
						while (true)
						{
							if (*p_tail == ',')
							{
								p_num_tail = p_tail;
								frequency.push_back(atoi(_term_buf_str.substr(int(p_num_head - &_term_buf_str[0]), int(p_num_tail - p_num_head)).c_str()));
								p_num_head = p_tail + 1;
							}
							if (*p_tail != '\"')
								p_tail++;
							else if (*p_tail == '\"' && *(p_tail + 1) == '\"')
								p_tail += 2;
							else
							{
								p_tail++;
								loop = false;
							}
							if (loop == false)
							{
								break;
							}
						}
					}
					else
					{
						while (*p_tail != ',')
							p_tail++;
					}

					// doc_id
					doc_id.resize(0);
					loop = true;
					p_head = p_tail + 1;
					p_tail = p_head + 1;
					p_num_head = p_head;
					if (*p_head == '\"')
					{
						p_head++;
						while (true)
						{
							if (*p_tail == ',')
							{
								p_num_tail = p_tail;
								doc_id.push_back(atoi(_term_buf_str.substr(int(p_num_head - &_term_buf_str[0]), int(p_num_tail - p_num_head)).c_str()));
								p_num_head = p_tail + 1;
							}
							if (*p_tail != '\"')
								p_tail++;
							else if (*p_tail == '\"' && *(p_tail + 1) == '\"')
								p_tail += 2;
							else
							{
								p_tail++;
								loop = false;
							}
							if (loop == false)
							{
								break;
							}
						}
					}
					else
					{
						while (*p_tail != ',')
							p_tail++;
					}

					_dic_term.insert(term, atoi(term_id.c_str()), frequency, doc_id);

					p_head = p_tail + 2;
				}
				
			}
			if (PROCESS_INFO)std::cout << "[Scratch Search Notice] There are " << _doc_id << " pages in total" << endl;
			if (PROCESS_INFO)std::cout << "[Scratch Search Notice] There are " << _term_id << " terms in total" << endl;
			clock_t end = clock();
			if (PROCESS_INFO)std::cout  << "[Scratch Search Notice] time : " << ((double)end - start) / CLOCKS_PER_SEC << "s" << endl;
			return;
		}
		default: return;
		}
	}
	void Search_Engine::search()
	{
		if (!(DEBUG_INFO || PROCESS_INFO))system("cls");
		logo();
		string show = "[Scratch Search Engine] \033[31;1mNot Find! Try Again\033[0m\n";
		while (true)
		{
			trie::trie dic_key_word;
			vector<Doc_Unit> doc_unit_vec;
			string content;
			string input_str;
			string term;
			int doc_id;
			string content_for_output;
			char* p_seg_head;
			char* p_seg_tail;
			char* p_term_head;
			char* p_term_tail;
			char* p_head;
			char* p_tail;
			bool exist = false;
			bool exit = false;
			std::cout  << "\n---------------------------------------------------------------------------------------------\n[Scratch Search Engine]";
			std::cout  << "         \033[30;1mSearch Anything! Enter -quit to stop the search engine\033[0m\r[Scratch Search Engine] \033[33;1mSearch:\033[0m ";
			if (!DEBUG_INFO) getline(cin, input_str);
			else { input_str = DEBGU_KEY_WORDS; std::cout  << input_str << endl << "[\033[32;1mScratch Search Debug\033[0m] Debug Mode: Search keywords provided! " << endl; }// test
			if (input_str == "-quit" || input_str == "-q")
			{
				break; // 退出搜索
			}
			transform(input_str.begin(), input_str.end(), input_str.begin(), ::tolower);
			p_head = &input_str[0];

			while (*p_head != '\0')
			{
				p_term_head = p_head;
				p_tail = p_head + 1;
				while (*p_tail != ' ' && *p_tail != '\0')p_tail++;
				p_term_tail = p_tail;
				while (p_term_head < p_term_tail)
				{
					if (isalnum(*p_term_head) == 0)p_term_head++;
					else break;
				}
				while (p_term_head < p_term_tail)
				{
					if (isalnum(*(p_term_tail - 1)) == 0)p_term_tail--;
					else break;
				}
				if (p_term_head < p_term_tail)
				{
					term = input_str.substr(int(p_term_head - &input_str[0]), int(p_term_tail - p_term_head));
					auto find = _dic_term.search(term);
					if (find)
					{
						exist = true;
						find = dic_key_word.search(term, trie::SearchMode::count);
						if (!find)
							dic_key_word.insert(term);
						else
							dic_key_word.check(find);
					}
					else
					{
						if (*p_tail == '\0')break;
						p_head = p_tail + 1;
						continue;
					}
				}
				if (*p_tail == '\0')break;
				p_head = p_tail + 1;
			}
			//int page_total = 0;
			int page = 0;
			dic_key_word.traversal(doc_unit_vec, _dic_term);
			merge_sort(doc_unit_vec, doc_unit_vec.size());
			auto page_total = doc_unit_vec.size() / DISPLY_RANGE + (doc_unit_vec.size() % DISPLY_RANGE == 0 ? 0 : 1);
			page = 0;
			while (true)
			{
				if (exist == true)
				{
					auto end = ((page + 1) * DISPLY_RANGE) < doc_unit_vec.size() ? ((page + 1) * DISPLY_RANGE) : doc_unit_vec.size();


					for (int i = page * DISPLY_RANGE; i < end; i++)
					{
						if (i >= doc_unit_vec.size())break;
						doc_id = doc_unit_vec[i].doc_id;
						content = _dic_doc[doc_id].content;
						transform(content.begin(), content.end(), content.begin(), ::tolower);
						for (int j = 0; j < doc_unit_vec[i].term_vec.size(); j++)
						{
							if(content.find(doc_unit_vec[i].term_vec[j]) == -1)break;
							p_seg_head = &_dic_doc[doc_id].content[content.find(doc_unit_vec[i].term_vec[j])];
							p_seg_tail = &_dic_doc[doc_id].content[content.find(doc_unit_vec[i].term_vec[j]) + doc_unit_vec[i].term_vec[j].size()];
							for (int i = 10; i > 0; i--)
							{
								if ((p_seg_head - 1) == &_dic_doc[doc_id].content[0] || (p_seg_head) == &_dic_doc[doc_id].content[0])
									break;
								while (*(p_seg_head - 1) != ' ' && (p_seg_head - 1) != &_dic_doc[doc_id].content[0])
									p_seg_head--;
								p_seg_head--;
							}
							for (int i = 30; i > 0; i--)
							{
								if (*p_seg_tail == '\0')
									break;
								while (*p_seg_tail != ' ' && *(p_seg_tail + 1) != '\0')
									p_seg_tail++;
								p_seg_tail++;
							}
							if (j == 0)content_for_output = "";
							content_for_output += "..." + _dic_doc[doc_id].content.substr(int(p_seg_head - &_dic_doc[doc_id].content[0]), int(p_seg_tail - p_seg_head)) + "...\n";
						}
						for (int j = 0; j < doc_unit_vec[i].term_vec.size(); j++)
						{
							regex pattern("(" + doc_unit_vec[i].term_vec[j] + ")", regex::icase);
							content_for_output = regex_replace(content_for_output, pattern, "\033[33;1m$1\033[30;1m");
						}
						if (i == page * DISPLY_RANGE)show = "";
						show += "\n\033[33;1mTitle\033[0m: "
							+ ((_dic_doc[doc_id].title[0] == '\"' && _dic_doc[doc_id].title[_dic_doc[doc_id].title.size() - 1] == '\"') ? _dic_doc[doc_id].title.substr(1, _dic_doc[doc_id].title.size() - 2) : _dic_doc[doc_id].title) + "\n" 
							+ "\033[33;1mUrl\033[0m: \033[34;1m" + _dic_doc[doc_id].url + "\033[0m\n"
							+ "\033[30;1m" + content_for_output + "\033[0m";
						if (i == doc_unit_vec.size() - 1)
						{
							show += "\033[33;1mNo More!\033[0m\n";
						}
					}
				}
				{
 					regex pattern(",{2,}");
					show = regex_replace(show, pattern, "");
				}
				{
					regex pattern("[_*]");
					show = regex_replace(show, pattern, "");
				}
				{
					regex pattern("\"\"");
					show = regex_replace(show, pattern, "\"");
				}
				{
					regex pattern("\'\'");
					show = regex_replace(show, pattern, "\'");
				}
				std::cout  << "\n[Scratch Search Engine] \033[33;1mResult:\033[0m current page " + to_string(page + 1) + ", total page " + to_string(page_total)
					+ "\n---------------------------------------------------------------------------------------------\n";
				std::cout  << show;
				std::cout  << "---------------------------------------------------------------------------------------------\n";
				if (show == "[Scratch Search Engine] \033[31;1mNot Find! Try Again\033[0m\n")
				{
					system("pause");
					std::cout  << endl;
					exit = true;
				}
				while (!exit) // command
				{
					string command;
					std::cout  << "                                 \033[30;1mEnter -help to get support\033[0m\r[Scratch Search Engine] \033[33;1mCommand:\033[0m ";
					cin >> command;
					if (command == "-page" || command == "-p")
					{
						cin >> command;
						if (command != "next" && command != "last")
						{
							int temp_num = atoi(command.c_str()) - 1;
							if (temp_num < 0 || temp_num > page_total)
							{
								std::cout  << "[Scratch Search Engine] \033[31;1mERROR\033[0m: page index out of range!\n";
								std::cout  << "[Scratch Search Engine] \033[33;1mCommand:\033[0m ";
								continue;
							}
							else
							{
								getline(cin, command);
								if (command != "")
								{
									std::cout  << "[Scratch Search Engine] \033[31;1mERROR\033[0m: page syntax error!\n";
									continue;
								}
								page = temp_num;
								break;
							}
						}
						else if (command == "next")
						{
							if (page + 1 >= page_total)
							{
								std::cout  << "[Scratch Search Engine] \033[31;1mERROR\033[0m: this is the last page!\n";
								continue;
							}
							else
							{
								getline(cin, command);
								if (command != "")
								{
									std::cout  << "[Scratch Search Engine] \033[31;1mERROR\033[0m: page syntax error!\n";
									continue;
								}
								page++;
								break;
							}
						}
						else if (command == "last")
						{
							if (page - 1 < 0)
							{
								std::cout  << "[Scratch Search Engine] \033[31;1mERROR\033[0m: this is the first page!\n";
								continue;
							}
							else
							{
								getline(cin, command);
								if (command != "")
								{
									std::cout  << "[Scratch Search Engine] \033[31;1mERROR\033[0m: page num<-- syntax error!\n";
									continue;
								}
								page--;
								break;
							}
						}
					}
					else if (command == "-exit")
					{
						getline(cin, command);
						if (command != "")
						{
							std::cout  << "[Scratch Search Engine] \033[31;1mERROR\033[0m: exit<-- syntax error!\n";
							continue;
						}
						exit = true;
						break;
					}
					else if (command == "-help" || command == "-h")
					{
						getline(cin, command);
						if (command != "")
						{
							std::cout  << "[Scratch Search Engine] \033[31;1mERROR\033[0m: help<-- syntax error!\n";
							continue;
						}
						std::cout  << "[Scratch Search Engine] \033[33;1mHelp\033[0m: \n \033[30;1m-page [page_num]: jump to [page_num]\n -page next: jump to the next page\n -page last: jump to the previous page\n -p: the same as commmand page\n -exit: end this search\n -help: show help\n -h: the same as command help\033[0m\n";
						continue;
					}
					else
					{
						std::cout  << "[Scratch Search Engine] \033[31;1mERROR\033[0m: no such command!\n";
					}
				}
				if (exit == true)break;
			}
			if (DEBUG_INFO) break; // test
			dic_key_word.clear();
			system("cls");
			logo();
			show = "[Scratch Search Engine] \033[31;1mNot Find! Try Again\033[0m\n";
		}
		_dic_term.clear();
		std::cout  << "[Scratch Search Engine] \033[33;1mThanks For Using!\033[0m" << endl;
		return;
	}
	void Search_Engine::merge_sort(vector<Doc_Unit>& doc_sort, int len)
	{
		vector <Doc_Unit> a = doc_sort;
		vector <Doc_Unit> b(len);
		for (int seg = 1; seg < len; seg += seg)
		{
			for (int start = 0; start < len; start += seg + seg) {
				int low = start, mid = min(start + seg, len), high = min(start + seg + seg, len);
				int k = low;
				int start1 = low, end1 = mid;
				int start2 = mid, end2 = high;
				while (start1 < end1 && start2 < end2)
					b[k++] = a[start1].doc_frequency > a[start2].doc_frequency ? a[start1++] : a[start2++];
				while (start1 < end1)
					b[k++] = a[start1++];
				while (start2 < end2)
					b[k++] = a[start2++];
			}
			vector <Doc_Unit> temp = a;
			a = b;
			b = temp;
		}
		doc_sort = a;
		return;
	}
	bool Search_Engine::is_file_exist_stat(string& file_address)
	{
		struct stat buffer;
		return (stat(file_address.c_str(), &buffer) == 0);
	}
	void Search_Engine::logo()
	{
		std::cout  << "\033[0m\n"
			<< "  +---------------------------------------------------------------------------------------+       " << endl
			<< "  | +-------------------------------------------------------------------------------------|-+     " << endl
			<< "  | |\033[33;1m         _________                    __         .__                                 \033[0m| |     " << endl
			<< "  | |\033[33;1m        /   _____/ ________________ _/  |_  ____ |  |__          __________ ______   \033[0m| |     " << endl
			<< "  | |\033[33;1m        \\_____  \\_/ ___\\_  __ \\__  \\\\   __\\/ ___\\|  |  \\        / ___/ ___// ____/   \033[0m| |     " << endl
			<< "  | |\033[33;1m        /        \\  \\___|  | \\// __ \\|  | \\  \\___|   \\  \\       \\__ \\\\__ \\/ __/      \033[0m| |     " << endl
			<< "  | |\033[33;1m       /_______  /\\___  >__|  (____  /__|  \\___  >___|  /      /____/____/ /___      \033[0m| |     " << endl
			<< "  | |\033[33;1m               \\/     \\/           \\/          \\/     \\/  ____ _/ /___  /_____/      \033[0m| |     " << endl
			<< "  | |\033[33;1m    _________                           .__              / __ `/ / __ \\/ /_  ____ _  \033[0m| |     " << endl
			<< "  | |\033[33;1m   /   _____/ ____ _____ _______   ____ |  |__          / /_/ / / /_/ / __ \\/ __ `/  \033[0m| |     " << endl
			<< "  | |\033[33;1m   \\_____  \\_/ __ \\\\__  \\\\_  __ \\_/ ___\\|  |  \\         \\__,_/_/ .___/ / / / /_/ /   \033[0m| |     " << endl
			<< "  | |\033[33;1m   /        \\  ___/ / __ \\|  | \\/\\  \\___|   \\  \\              / /   /_/ /_/\\__,_/    \033[0m| |     " << endl
			<< "  | |\033[33;1m  /_______  /\\___  >____  /__|    \\___  >___|  /             /_/                     \033[0m| |     " << endl
			<< "  | |\033[33;1m          \\/     \\/     \\/            \\/     \\/                                      \033[0m| |     " << endl
			<< "  +-|-------------------------------------------------------------------------------------+ |     " << endl
			<< "    +---------------------------------------------------------------------------------------+     " << endl
			<< "\033[0m";
		// Font: Graffiti, Slant
		return;
	}
}

