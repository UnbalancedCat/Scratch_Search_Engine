#include <iostream>
#include "Scratch_Search_Engine.h"
using namespace std;

int main()
{
	clock_t start = clock();
	sse::Search_Engine scratch_search;
	scratch_search.start();
	clock_t end = clock();
	if (sse::PROCESS_INFO)std::cout  << "[Scratch Search Notice] Total time : " << ((double)end - start) / CLOCKS_PER_SEC << "s" << endl;
	return 0;
}