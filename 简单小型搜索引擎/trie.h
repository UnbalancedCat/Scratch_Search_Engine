#pragma once

#include <iostream>
#include <iterator>
#include <sstream>
#include <string>
#include <vector>
#include<cmath>
using namespace std;

struct Doc_Unit
{
	int doc_id = -1;
	int doc_frequency = 0;
	vector<string> term_vec;
	vector<int> term_frequency;
};

namespace trie
{
	enum SearchMode
	{
		normal,
		count,
	};
	
	constexpr auto BEGIN = 0;
	constexpr auto RANGE = 128;
	struct TrieNode
	{
		TrieNode* next[RANGE];
		TrieNode* next_term;
		bool end;
		int path;
		
		string term;
		int frequency_pre_doc;
		int frequency_total; 
		int doc_id;
		int term_id;
		vector<int>frequency_vec;
		vector<int>doc_id_vec;
		TrieNode() // ��ʼ�����
		{
			end = false;
			path = 0;
			frequency_pre_doc = 0;
			frequency_total = 0;
			term_id = -1;
			doc_id = -1;
			for (int i = 0; i < RANGE; i++)
				next[i] = nullptr;
			next_term = nullptr;
		}
	};

	class trie
	{
	private:
		TrieNode* _root;
		TrieNode* _tail;
	public:
		trie()
		{
			_root = new TrieNode;
			_tail = _root;
		}
		void insert(string word, int term_id = -1,int doc_id = -1);
		void insert(string word, int term_id, vector<int>& frequency, vector<int>& doc_id);
		void remove(string word);
		TrieNode* search(string word, SearchMode mode = SearchMode::normal);
		bool check(TrieNode* node, int doc_id = -1);
		void traversal(string &buf_str);
		void traversal(vector<Doc_Unit>& doc_vec, trie& dic_term);
		void traversal();
		void clear(TrieNode* node);
		void clear();
		void merge_sort(vector<int>& frequency_vec, vector<int>& doc_id_vec, int len);
	};

	void trie::insert(string word, int term_id, int doc_id)
	{
		if (word == "")
			return;
		int index = 0;
		TrieNode* node = _root;
		for (int i = 0; i < word.length(); i++)
		{
			index = word[i] - BEGIN;
			if (index < BEGIN || index >= RANGE)return;
			if (!node->next[index])
			{
				node->next[index] = new TrieNode;
				node = node->next[index];
				node->path++;
			}
			else node = node->next[index];
		}
		node->end = true;
		node->term = word;
		node->term_id = term_id;
		node->frequency_total++;
		node->frequency_pre_doc++;
		node->doc_id = doc_id;
		_tail->next_term = node;
		_tail = node;
		return;
	}
	void trie::insert(string word, int term_id, vector<int>& frequency, vector<int>& doc_id)
	{
		if (word == "")
			return;
		int index = 0;
		TrieNode* node = _root;
		for (int i = 0; i < word.length(); i++)
		{
			index = word[i] - BEGIN;
			if (index < BEGIN || index >= RANGE)return;
			if (!node->next[index])
			{
				node->next[index] = new TrieNode;
				node = node->next[index];
				node->path++;
			}
			else node = node->next[index];
		}
		node->end = true;
		node->term = word;
		node->term_id = term_id;
		node->frequency_vec = frequency;
		node->doc_id_vec = doc_id;
		_tail->next_term = node;
		_tail = node;
		return;
	}
	void trie::remove(string word)
	{
		if (!search(word, SearchMode::normal))
			return;
		int index = 0;
		TrieNode* node = _root;
		for (int i = 0; i < word.length(); i++)
		{
			index = word[i] - BEGIN;
			if (--node->path == 0)
			{
				clear(node);
				node = nullptr;
				return;
			}
			node = node->next[index];
		}
		node->end = true;
		return;
	}
	TrieNode* trie::search(string word, SearchMode mode)
	{
		if (word == "")
			return nullptr;
		TrieNode* node = _root;
		int index = 0;
		for (int i = 0; i < word.length(); i++)
		{
			index = word[i] - BEGIN;
			if (index >= RANGE || index < BEGIN || node == nullptr || node->next[index] == nullptr)
				return nullptr;
			node = node->next[index];
		}
		if (node->end == false)
			return nullptr;
		else
		{
			switch (mode)
			{
			case SearchMode::normal: break;
			case SearchMode::count:node->frequency_total++; node->frequency_pre_doc++; break;
			}
			return node;
		}
	}
	bool trie::check(TrieNode* node, int doc_id)
	{
		if (node->doc_id == doc_id) return false;
		else
		{
			node->doc_id = doc_id;
			node->doc_id_vec.push_back(doc_id);
			node->frequency_vec.push_back(node->frequency_pre_doc);
			node->frequency_pre_doc = 0;
			return true;
		}
	}
	void trie::traversal(string& buf_str)
	{
		TrieNode* cur = _root->next_term;
		buf_str = "term_id,term,frequency,doc_id,\n";
		stringstream temp;
		string frequency;
		string doc_id;

		while (cur != nullptr)
		{
			//cur->frequency_vec.push_back(cur->frequency_pre_doc); cur->frequency_pre_doc = 0;
			//cur->doc_id_vec.push_back(cur->doc_id);
			merge_sort(cur->frequency_vec, cur->doc_id_vec, cur->doc_id_vec.size());
			copy(cur->frequency_vec.begin(), cur->frequency_vec.end(), ostream_iterator<int>(temp, ","));
			frequency = temp.str();
			temp.clear();
			temp.str("");
			copy(cur->doc_id_vec.begin(), cur->doc_id_vec.end(), ostream_iterator<int>(temp, ","));
			doc_id = temp.str();
			temp.clear();
			temp.str("");

			buf_str += (to_string(cur->term_id) + ",\"" + cur->term + "\",\"" + frequency + "\",\"" + doc_id + "\",\n");
			cur = cur->next_term;
		}
		return;
	}
	void trie::traversal(vector<Doc_Unit>&doc_vec, trie& dic_term)
	{
		TrieNode* cur = _root->next_term;

		while (cur != nullptr)
		{
			//cur->frequency_vec.push_back(cur->frequency_pre_doc); cur->frequency_pre_doc = 0;
			//cur->doc_id_vec.push_back(cur->doc_id);
			merge_sort(cur->frequency_vec, cur->doc_id_vec, cur->doc_id_vec.size());

			auto node = dic_term.search(cur->term);
			auto length = node->doc_id_vec.size();
			for (int i = 0; i < length; i++)
			{
				auto doc_id = node->doc_id_vec[i];
				if (doc_id >= doc_vec.size())doc_vec.resize(doc_id + 1);
				doc_vec[doc_id].doc_id = doc_id;
				doc_vec[doc_id].term_vec.push_back(node->term);
				doc_vec[doc_id].term_frequency.push_back(node->frequency_vec[i]);
				doc_vec[doc_id].doc_frequency++;
			}

			cur = cur->next_term;
		}
		
		for(auto iter = doc_vec.begin(); iter < doc_vec.end();)
		{
			if (iter->doc_id == -1)iter = doc_vec.erase(iter);
			else iter++;
		}
		return;
	}
	void trie::traversal()
	{
		TrieNode* cur = _root->next_term;

		while (cur != nullptr)
		{
			cur->frequency_vec.push_back(cur->frequency_pre_doc); cur->frequency_pre_doc = 0;
			cur->doc_id_vec.push_back(cur->doc_id);
			merge_sort(cur->frequency_vec, cur->doc_id_vec, cur->doc_id_vec.size());
			cur = cur->next_term;
		}
		return;
	}
	void trie::clear()
	{
		TrieNode* node = _root;
		if (node == nullptr)
			return;
		for (int num = 0; num < RANGE; num++)
		{
			if (node && node->next[num])
			{
				clear(node->next[num]);
				node = nullptr;
			}
		}
		return;
	}
	void trie::clear(TrieNode* node)
	{
		if (node == nullptr)
			return;
		for (int num = 0; num < RANGE; num++)
		{
			if (node && node->next[num])
			{
				clear(node->next[num]);
				node = nullptr;
			}
		}
		return;
	}
	void trie::merge_sort(vector<int>& frequency_vec, vector<int>& doc_id_vec, int len)
	{
		vector <int> a_f = frequency_vec;
		vector <int> b_f(len);
		vector <int> a_d = doc_id_vec;
		vector <int> b_d(len);
		for (int seg = 1; seg < len; seg += seg)
		{
			for (int start = 0; start < len; start += seg + seg) 
			{
				int low = start, mid = min(start + seg, len), high = min(start + seg + seg, len);
				int k = low;
				int start1 = low, end1 = mid;
				int start2 = mid, end2 = high;
				while (start1 < end1 && start2 < end2)
				{
					if (a_f[start1] > a_f[start2])
					{
						b_f[k] = a_f[start1];
						b_d[k++] = a_d[start1++];
					}
					else
					{
						b_f[k] = a_f[start2];
						b_d[k++] = a_d[start2++];
					}
					
				}
				while (start1 < end1)
				{
					b_f[k] = a_f[start1];
					b_d[k++] = a_d[start1++];
				}
				while (start2 < end2)
				{
					b_f[k] = a_f[start2];
					b_d[k++] = a_d[start2++];
				}
			}
			vector <int> temp_f = a_f;
			vector <int> temp_d = a_d;
			a_f = b_f;
			a_d = b_d;
			b_f = temp_f;
			b_d = temp_d;
		}
		frequency_vec = a_f;
		doc_id_vec = a_d;
		return;
	}
}