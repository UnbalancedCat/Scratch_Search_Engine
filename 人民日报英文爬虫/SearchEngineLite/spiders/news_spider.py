import scrapy


class NewsSpider(scrapy.Spider):
    name = "news"

    start_urls = [
        'http://en.people.cn/n3/2022/0615/c90000-10109644.html'
    ]

    def parse(self, response, **kwargs):
        for news in response.css('div.main'):
            if news.css('h1::text').get() is not None:
                yield {
                    'url': response.url,
                    'title': news.css('h1::text').get(),
                    'news_content': ','.join(news.css('p::text').getall()),
                }
        anchors_a = response.css('div.relevant_news a')
        yield from response.follow_all(anchors_a, callback=self.parse)
